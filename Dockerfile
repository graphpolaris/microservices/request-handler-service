# docker buildx build --platform linux/amd64,linux/arm64 -t datastropheregistry.azurecr.io/request-handler-service . --push --ssh default
# STAGE 1
FROM golang:1.16

ARG TARGETARCH
ARG TARGETOS

WORKDIR /app

# Use SSH instead of HTTPS
RUN echo "[url \"git@git.science.uu.nl:\"]\n\tinsteadOf = https://git.science.uu.nl/" >> /root/.gitconfig

# Turn off strict host key checking
RUN mkdir /root/.ssh && echo "StrictHostKeyChecking no " > /root/.ssh/config

# Copy go files into the image
COPY go.mod ./
COPY go.sum ./
COPY cmd/ ./cmd/
COPY internal/ ./internal/

# Gather dependencies
RUN --mount=type=ssh go mod download

# Compile for the target architecture and operating system
# Add SSH mount as this operation requires access to private repos
RUN GOARCH=${TARGETARCH} GOOS=${TARGETOS} CGO_ENABLED=0 go build -o ./main ./cmd/request-handler-service/

# STAGE 2
FROM busybox

WORKDIR /app

# Copy the built binary into this image
COPY --from=0 /app/main ./

# Run the binary
CMD ./main