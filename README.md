[![License](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)
[![Pipeline Status main](https://git.science.uu.nl/datastrophe/microservices-backbone/request-handler-service/badges/main/pipeline.svg?key_text=pipeline%20main&key_width=90)](https://git.science.uu.nl/datastrophe/microservices-backbone/request-handler-service/-/pipelines)
[![Pipeline Status develop](https://git.science.uu.nl/datastrophe/microservices-backbone/request-handler-service/badges/develop/pipeline.svg?key_text=pipeline%20develop&key_width=105)](https://git.science.uu.nl/datastrophe/microservices-backbone/request-handler-service/-/pipelines)
[![Coverage main](https://git.science.uu.nl/datastrophe/microservices-backbone/request-handler-service/badges/main/coverage.svg?key_text=coverage%20main&key_width=94)](https://git.science.uu.nl/datastrophe/microservices-backbone/request-handler-service/-/commits/main)
[![Coverage develop](https://git.science.uu.nl/datastrophe/microservices-backbone/request-handler-service/badges/develop/coverage.svg?key_text=coverage%20develop&key_width=110)](https://git.science.uu.nl/datastrophe/microservices-backbone/request-handler-service/-/commits/develop)

<div align='center'>
<img src="https://git.science.uu.nl/datastrophe/frontend/-/raw/develop/src/presentation/view/navbar/logogp.png" align="center" width="150" alt="Project icon">
</div>

## Request Handler Service
This service handles incoming requests and publishes them into correct message queues. Currently it only does this for schema requests. Its functionality for query requests has been taken over by the [query orchestrator](https://git.science.uu.nl/datastrophe/microservices-backbone/query-orchestrator), and something similar should be done for schema requests. The service can be seen as soon to be deprecated.

### Dev Dependencies
Here are the developer dependencies. Coding can be done without these tools, but they sure make your life easier.
- Make
- Docker

### Dependencies
This service depends on quite some other services, as it is essentially just a shell for the query conversion and execution packages.
- RabbitMQ
- User management service (to retrieve a database type)

### Environment Variables
Here are the environment variables that are used in this service:
- `RABBIT_HOST` address of the RabbitMQ instance (string)
- `RABBIT_PORT` RabbitMQ port (int)
- `RABBIT_USER` RabbitMQ username (string)
- `RABBIT_PASSWORD` RabbitMQ password (string)
- `LOG_MESSAGES` Whether to log messages (bool)
- `JWT_SECRET` The secret used to validate JWT's (string)

### Building the Service
To compile the service into a single binary, the following commands can be used. Go can build for many platforms, so we have three build commands, for the three main platforms.
```
make linux
make windows
make macos
```
To build the service and push it to the Docker registry the `make docker` command can be used.

### Testing and Coverage
To test the `make test` command can be used. It will run every test in the repo. To get the code coverage the command `make coverage` can be used. This command will run all tests and display total coverage over all files. It will also generate an html file which displays exactly which lines have been covered.

### Kubernetes
All the Kubernetes config files live in the `/deployments` directory. This service has a basic deployment which sets all environment variables, some of which from Kubernetes secrets. The service also has a Kubernetes service config for the Ingress to be able to route traffic to it.