/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package messagequeue

import (
	"os"
	"request-handler-service/internal/errorhandler"
	"strconv"
	"time"

	"github.com/christinoleo/alice"
)

/*
creates a broker

	Return: *alice.RabbitBroker, returns a rabbit broker
*/
func createBroker() *alice.RabbitBroker {
	// Create connection config using environment variables
	rabbitUser := os.Getenv("RABBIT_USER")
	rabbitPassword := os.Getenv("RABBIT_PASSWORD")
	rabbitHost := os.Getenv("RABBIT_HOST")
	rabbitPort, err := strconv.Atoi(os.Getenv("RABBIT_PORT"))
	if err != nil {
		errorhandler.FailWithError(err, "port should be a number")
	}

	config := alice.CreateConfig(rabbitUser, rabbitPassword, rabbitHost, rabbitPort, true, time.Minute*1, alice.DefaultConsumerErrorHandler)

	broker, err := alice.CreateBroker(config)
	return broker
}
