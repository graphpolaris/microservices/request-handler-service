/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package messagequeue

import (
	"fmt"
	"request-handler-service/internal/drivers/rpcdriver"

	"github.com/christinoleo/alice"
	amqp "github.com/rabbitmq/amqp091-go"
)

var broker *alice.RabbitBroker
var queryRequestProducer alice.Producer
var schemaRequestProducer alice.Producer
var rpcDriver rpcdriver.Interface

/*
Start starts all the producers
*/
func Start() {
	rpcDriver = rpcdriver.New()
	broker = createBroker()
	startQueryRequestProducer()
	startSchemaRequestProducer()
}

/*
Start schema request producer
*/
func startQueryRequestProducer() {
	exchange, err := alice.CreateDefaultExchange("requests-exchange", alice.Direct)
	if err != nil {
		fmt.Println(err)
	}

	queryRequestProducer, _ = broker.CreateProducer(exchange, alice.DefaultProducerErrorHandler)
}

/*
Query publishes a query request

	request: []byte, the query request,
	sessionID: *string, the ID of the session,
	clientID: *string, the ID of the client
*/
func Query(request []byte, sessionID *string, clientID *string) {
	key := "aql-query-request"

	headers := amqp.Table{}
	headers["sessionID"] = *sessionID
	headers["clientID"] = *clientID
	queryRequestProducer.PublishMessage(request, &key, &headers)
}

/*
Start schema request producer
*/
func startSchemaRequestProducer() {
	// Define the exchange we want to produce to
	exchange, err := alice.CreateDefaultExchange("requests-exchange", alice.Direct)
	if err != nil {
		fmt.Println(err)
	}

	// Create the producer bound to the requests-exchange exchange
	schemaRequestProducer, _ = broker.CreateProducer(exchange, alice.DefaultProducerErrorHandler)
}

/*
Schema produces the request to the exchange

	request: []byte, the request of the schema,
	sessionID: *string, the ID of the session,
	clientID: *string, the ID of the client
*/
func Schema(request []byte, sessionID *string, clientID *string, databaseName *string) {
	// Send the message to the correct queue
	// Request the database type from the user management service
	databaseType, err := rpcDriver.GetDatabaseType(clientID, databaseName)
	if err != nil {
		return
	}

	headers := amqp.Table{}
	headers["sessionID"] = *sessionID
	headers["clientID"] = *clientID

	switch *databaseType {
	case "arangodb":
		key := "arangodb-schema-request"
		schemaRequestProducer.PublishMessage(request, &key, &headers)
	case "neo4j":
		key := "neo4j-schema-request"
		schemaRequestProducer.PublishMessage(request, &key, &headers)
	}
}

/*
Database produces a database request

	request: []byte, the request of the database
	sessionID: *string, the ID of the session
*/
func Database(request []byte, sessionID *string) {
	key := "database"
	headers := amqp.Table{}
	headers["sessionID"] = *sessionID

	schemaRequestProducer.PublishMessage(request, &key, &headers)
}
