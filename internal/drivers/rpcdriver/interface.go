/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package rpcdriver

/*
Interface specifies the methods a rpc driver should implement
*/
type Interface interface {
	GetDatabaseType(clientID *string, databaseName *string) (*string, error)
}
