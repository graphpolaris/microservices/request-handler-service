/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package rpcdriver

import (
	"context"
	"errors"
	"os"
	"request-handler-service/internal/drivers/rpcdriver/databaseTypeService"

	"google.golang.org/grpc"
)

/*
GetDatabaseType opens a gRPC connection to the user management service and retrieves the database info for the given client and database name
	clientID: *string, the ID of the client
	databaseName: *string, the name of the database
	Return: (*string, error), the database type and a potential error
*/
func (driver *Driver) GetDatabaseType(clientID *string, databaseName *string) (*string, error) {
	userManagementServiceAddress := "user-management-service:9000"
	if os.Getenv("DEV") == "true" {
		userManagementServiceAddress = "localhost:9000"
	}
	conn, err := grpc.Dial(userManagementServiceAddress, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	grpcClient := databaseTypeService.NewDatabaseTypeServiceClient(conn)

	response, err := grpcClient.GetDatabaseType(context.Background(), &databaseTypeService.DatabaseTypeRequest{ClientID: *clientID, DatabaseName: *databaseName})
	if err != nil {
		return nil, err
	}

	var databaseType string
	switch response.DatabaseType {
	case databaseTypeService.DatabaseTypeResponse_ARANGODB:
		databaseType = "arangodb"
	case databaseTypeService.DatabaseTypeResponse_NEO4J:
		databaseType = "neo4j"
	default:
		return nil, errors.New("Unknown database type returned")
	}

	return &databaseType, nil
}
