/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package rpcdriver

/*
A Driver implements the rpc driver interface
*/
type Driver struct {
}

/*
New creates a new rpc driver
*/
func New() Interface {
	return &Driver{}
}
