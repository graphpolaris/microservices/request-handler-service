/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package validate

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

var testDatabaseCredentials DatabaseCredentials

/*
Resets the variables for the tests
*/
func resetTestDatabaseCredentials() {
	url := "https://922ae80b981e.arangodb.cloud"
	port := 8529
	username := "root"
	password := "cUJ9P60fEP9NofOrLLtY"
	databaseType := "arangodb"

	testDatabaseCredentials = DatabaseCredentials{
		URL:      &url,
		Port:     &port,
		Username: &username,
		Password: &password,
		Type:     &databaseType,
	}
}

/*
Tests if there is no username
	t: *testing.T, makes go recognise this as a test
*/
func TestNoDatabaseUsername(t *testing.T) {
	resetTestDatabaseCredentials()
	testDatabaseCredentials.Username = nil
	assert.Equal(t, testDatabaseCredentials.Validate(), errors.New("no database username supplied"))
}

/*
Tests if there is no password
	t: *testing.T, makes go recognise this as a test
*/
func TestNoDatabasePassword(t *testing.T) {
	resetTestDatabaseCredentials()
	testDatabaseCredentials.Password = nil
	assert.Equal(t, testDatabaseCredentials.Validate(), errors.New("no database password supplied"))
}

/*
Tests if there is no database port
	t: *testing.T, makes go recognise this as a test
*/
func TestNoDatabasePort(t *testing.T) {
	resetTestDatabaseCredentials()
	testDatabaseCredentials.Port = nil
	assert.Equal(t, testDatabaseCredentials.Validate(), errors.New("no database port supplied"))
}

/*
Tests if there is no database url
	t: *testing.T, makes go recognise this as a test
*/
func TestNoDatabaseURL(t *testing.T) {
	resetTestDatabaseCredentials()
	testDatabaseCredentials.URL = nil
	assert.Equal(t, testDatabaseCredentials.Validate(), errors.New("no database url supplied"))
}

/*
Tests if there is no database type
	t: *testing.T, makes go recognise this as a test
*/
func TestNoDatabaseType(t *testing.T) {
	resetTestDatabaseCredentials()
	testDatabaseCredentials.Type = nil
	assert.Equal(t, testDatabaseCredentials.Validate(), errors.New("no database type provided"))
}

/*
Tests an unsupported database type
	t: *testing.T, makes go recognise this as a test
*/
func TestUnsupportedDatabaseType(t *testing.T) {
	resetTestDatabaseCredentials()
	invalidType := "invalid"
	testDatabaseCredentials.Type = &invalidType
	assert.Equal(t, testDatabaseCredentials.Validate(), errors.New("unsupported database type provided"))
}
