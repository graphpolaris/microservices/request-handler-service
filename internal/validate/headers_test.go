/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package validate

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

/*
Tests if there is no client ID
	t: *testing.T, makes go recognise this as a test
*/
func TestNoClientID(t *testing.T) {
	resetSchemaRequest()
	testSchemaRequest.Headers.ClientID = nil
	assert.Equal(t, testSchemaRequest.Headers.Validate(), errors.New("clientID required"))
}

/*
Tests with an invalid client ID
	t: *testing.T, makes go recognise this as a test
*/
func TestInvalidClientID(t *testing.T) {
	resetSchemaRequest()
	invalidClientID := "iminvalid"
	testSchemaRequest.Headers.ClientID = &invalidClientID
	assert.Equal(t, testSchemaRequest.Headers.Validate(), errors.New("invalid clientID"))
}

/*
Tests with no routing key
	t: *testing.T, makes go recognise this as a test
*/
func TestNoRoutingKey(t *testing.T) {
	resetSchemaRequest()
	testSchemaRequest.Headers.RoutingKey = nil
	assert.Equal(t, testSchemaRequest.Headers.Validate(), errors.New("routing key required"))
}

/*
Tests with an invalid routing key
	t: *testing.T, makes go recognise this as a test
*/
func TestInvalidRoutingKey(t *testing.T) {
	resetSchemaRequest()
	invalidRoutingKey := "iminvalid"
	testSchemaRequest.Headers.RoutingKey = &invalidRoutingKey
	assert.Equal(t, testSchemaRequest.Headers.Validate(), errors.New("invalid routing key"))
}

/*
Benchmarks the header validatin
	b: *testing.B, makes go recognise this as a test
*/
func BenchmarkHeaderValidation(b *testing.B) {
	resetSchemaRequest()

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		testSchemaRequest.Headers.Validate()
	}
}
