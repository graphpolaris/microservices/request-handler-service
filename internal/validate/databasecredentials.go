/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package validate

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

/*
DatabaseCredentials contains all the information needed to connect to a database
*/
type DatabaseCredentials struct {
	URL      *string `json:"url"`      // Database url
	Port     *int    `json:"port"`     // Database port
	Username *string `json:"username"` // Database username
	Password *string `json:"password"` // Database password
	Type     *string `json:"type"`     // Database type (Neo4j, ArangoDB, Neptune etc)
}

/*
Validate validates the provided database credentials
	Return: error, returns a potential error
*/
func (c *DatabaseCredentials) Validate() error {
	// Database username validation
	if c.Username == nil {
		return errors.New("no database username supplied")
	}

	// Database password validation
	if c.Password == nil {
		return errors.New("no database password supplied")
	}

	// Database port validation
	if c.Port == nil {
		return errors.New("no database port supplied")
	}

	// Database url validation
	if c.URL == nil {
		return errors.New("no database url supplied")
	}

	// Check if a database type is supplied
	if c.Type == nil {
		return errors.New("no database type provided")
	}

	// Check if the supplied database type is of one of the supported types
	if !isValidDatabaseType(*c.Type) {
		return errors.New("unsupported database type provided")
	}

	// Connect to database to determine if the credentials are valid
	switch *c.Type {
	case "arangodb":
		if err := c.testArangoConnection(); err != nil {
			return err
		}
	default:
	}

	return nil
}

/*
Checks if it is a valid database type
	t: string, the type of the database
	Return: bool, returns true if the database is supported
*/
func isValidDatabaseType(t string) bool {
	switch t {
	case
		"neo4j",
		"arangodb",
		"neptune":
		return true
	}
	return false
}

/*
Tests the connection of arango
	Return: error, returns a potential error
*/
func (c *DatabaseCredentials) testArangoConnection() error {
	// Create connection url
	url := *c.URL + ":" + fmt.Sprint(*c.Port) + "/_open/auth"

	requestBody, err := json.Marshal(map[string]string{
		"username": *c.Username,
		"password": *c.Password,
	})
	if err != nil {
		fmt.Println("Failed to marshall data")
	}

	response, err := http.Post(url, "application/json", bytes.NewBuffer(requestBody))
	if err != nil {
		fmt.Println("Failed to perform POST request to ArangoDB")
	}
	defer response.Body.Close()

	fmt.Println(response.StatusCode)

	if response.StatusCode != 200 {
		return errors.New("invalid ArangoDB credentials")
	}
	return nil
}
