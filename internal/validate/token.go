/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package validate

import "errors"

/*
TokenRequest models a request to create a JWT
*/
type TokenRequest struct {
	ClientID *string // The clientID that needs to be encapsulated in a JWT
}

/*
Validate validates a token creation request
	Return: error, returns a potential error
*/
func (tr *TokenRequest) Validate() error {
	if tr.ClientID == nil {
		return errors.New("no clientID provided")
	}

	if len(*tr.ClientID) != 20 {
		return errors.New("invalid clientID")
	}

	return nil
}
