/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package validate

import "errors"

/*
Headers contains clientID and routingKey both of which are used for routing
*/
type Headers struct {
	ClientID   *string `json:"clientID"`
	RoutingKey *string `json:"routingKey"`
}

/*
Validate validates the request headers
	Return: error, returns a potential error
*/
func (h *Headers) Validate() error {
	if h.ClientID == nil {
		return errors.New("clientID required")
	}
	if len(*h.ClientID) != 20 {
		return errors.New("invalid clientID")
	}
	if h.RoutingKey == nil {
		return errors.New("routing key required")
	}
	if len(*h.RoutingKey) != 20 {
		return errors.New("invalid routing key")
	}
	if len(*h.RoutingKey) < 10 {
		return errors.New("woopsie")
	}
	return nil
}
