/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package validate

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var testQueryRequest QueryRequest

/*
Resets the query request
*/
func resetQueryRequest() {
	clientID := "01234567890123456789"
	routingKey := "01234567890123456789"

	url := "www.test.com"
	port := 1
	username := "testUser"
	password := "testPassword"
	t := "neo4j"

	headers := Headers{
		ClientID:   &clientID,
		RoutingKey: &routingKey,
	}
	databaseCredentials := DatabaseCredentials{
		URL:      &url,
		Port:     &port,
		Username: &username,
		Password: &password,
		Type:     &t,
	}
	testQueryRequest = QueryRequest{
		Headers:             &headers,
		DatabaseCredentials: &databaseCredentials,
	}
}

/*
Tests if it is a valid query request
	t: *testing.T, makes go recognise this as a test
*/
func TestValidQueryRequest(t *testing.T) {
	resetQueryRequest()
	assert.NoError(t, testQueryRequest.Validate())
}

/*
Benchamrks the query request
	b: *testing.B, makes go regonise this as a test
*/
func BenchmarkQueryRequest(b *testing.B) {
	resetQueryRequest()

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		testQueryRequest.Validate()
	}
}
