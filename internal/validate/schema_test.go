/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package validate

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

var testSchemaRequest SchemaRequest

/*
Resets the schema request
*/
func resetSchemaRequest() {
	clientID := "01234567890123456789"
	routingKey := "01234567890123456789"

	key := "arango-schema"

	url := "www.test.com"
	port := 1
	username := "testUser"
	password := "testPassword"
	t := "neo4j"

	headers := Headers{
		ClientID:   &clientID,
		RoutingKey: &routingKey,
	}
	databaseCredentials := DatabaseCredentials{
		URL:      &url,
		Port:     &port,
		Username: &username,
		Password: &password,
		Type:     &t,
	}
	testSchemaRequest = SchemaRequest{
		Headers:             &headers,
		Key:                 &key,
		DatabaseCredentials: &databaseCredentials,
	}
}

/*
Tests if the schema request is valid
	t: *testing.T, makes go recognise this as a test
*/
func TestValidSchemaRequest(t *testing.T) {
	resetSchemaRequest()
	assert.NoError(t, testSchemaRequest.Validate())
}

/*
Tests if there is no schema key
	t: *testing.T, makes go recognise this as a test
*/
func TestNoSchemaKey(t *testing.T) {
	resetSchemaRequest()
	testSchemaRequest.Key = nil
	assert.Equal(t, testSchemaRequest.Validate(), errors.New("no request key supplied"))
}

/*
Tests if the schema is invalid
	t: *testing.T, makes go recognise this as a test
*/
func TestInvalidSchemaKey(t *testing.T) {
	resetSchemaRequest()
	invalidSchemaKey := "arango-schemaaa"
	testSchemaRequest.Key = &invalidSchemaKey
	assert.Equal(t, testSchemaRequest.Validate(), errors.New("invalid schema request key"))
}

/*
Tests with an arango schema key
	t: *testing.T, makes go recognise this as a test
*/
func TestArangoSchemaKey(t *testing.T) {
	resetSchemaRequest()
	arangoKey := "arango-schema"
	testSchemaRequest.Key = &arangoKey
	assert.NoError(t, testSchemaRequest.Validate())
}

/*
Tests with a neo4j routing key
	t: *testing.T, makes go recognise this as a test
*/
func TestNeo4jRoutingKey(t *testing.T) {
	resetSchemaRequest()
	neo4jKey := "neo4j-schema"
	testSchemaRequest.Key = &neo4jKey
	assert.NoError(t, testSchemaRequest.Validate())
}

/*
Tests with a neptune routing key
	t: *testing.T, makes go recognise this as a test
*/
func TestNeptuneRoutingKey(t *testing.T) {
	resetSchemaRequest()
	neptuneKey := "neptune-schema"
	testSchemaRequest.Key = &neptuneKey
	assert.NoError(t, testSchemaRequest.Validate())
}

/*
Tests with no database credentials
	t: *testing.T, makes go recognise this as a test
*/
func TestNoDatabaseCredentials(t *testing.T) {
	resetSchemaRequest()
	testSchemaRequest.DatabaseCredentials = nil
	assert.Equal(t, testSchemaRequest.Validate(), errors.New("no database credentials supplied"))
}

/*
Benchmarks the schema request validation
	b: *testing.B, makes go recognise this as a test
*/
func BenchmarkSchemaRequestValidation(b *testing.B) {
	resetSchemaRequest()

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		testSchemaRequest.Validate()
	}
}
