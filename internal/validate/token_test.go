/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package validate

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

var testCreateTokenRequest TokenRequest

/*
Resets the token request
*/
func resetTestCreateTokenRequest() {
	clientID := "01234567890123456789"
	testCreateTokenRequest = TokenRequest{
		ClientID: &clientID,
	}
}

/*
Tests if it is a valid request
	t: *testing.T, makes go recognise this as a test
*/
func TestValidRequest(t *testing.T) {
	resetTestCreateTokenRequest()
	assert.NoError(t, testCreateTokenRequest.Validate())
}

/*
Tests if there is no client ID
	t: *testing.T, makes go recognise this as a test
*/
func TestTokenNoClientID(t *testing.T) {
	resetTestCreateTokenRequest()
	testCreateTokenRequest.ClientID = nil
	assert.Equal(t, testCreateTokenRequest.Validate(), errors.New("no clientID provided"))
}

/*
Tests if there cliend ID is invalid
	t: *testing.T, makes go recognise this as a test
*/
func TestTokenClientIDInvalid(t *testing.T) {
	resetTestCreateTokenRequest()
	invalidClientID := "iminvalid"
	testCreateTokenRequest.ClientID = &invalidClientID
	assert.Equal(t, testCreateTokenRequest.Validate(), errors.New("invalid clientID"))
}

/*
Benchmarks the token creation
	b: *testing.B, makes go recognise this as a test
*/
func BenchmarkTokenCreation(b *testing.B) {
	resetTestCreateTokenRequest()

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		testCreateTokenRequest.Validate()
	}
}
