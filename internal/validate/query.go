/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package validate

/*
QueryRequest models a query request
*/
type QueryRequest struct {
	Headers             *Headers             `json:"headers"`
	DatabaseCredentials *DatabaseCredentials `json:"databaseCredentials"`
}

/*
Validate checks if a request is valid
	Return: error, returns a potential error
*/
func (qr QueryRequest) Validate() error {
	if err := qr.Headers.Validate(); err != nil {
		return err
	}

	if err := qr.DatabaseCredentials.Validate(); err != nil {
		return err
	}

	return nil
}
