/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package validate

import (
	"errors"
)

/*
SchemaRequest models a schema request
*/
type SchemaRequest struct {
	Headers             *Headers             `json:"headers"` // Required request headers
	Key                 *string              `json:"key"`     // The key for this request: aql-schema, neo4j-schema etc
	DatabaseCredentials *DatabaseCredentials `json:"databaseCredentials"`
}

/*
Validate checks if a request is valid
	Return: error, returns a potential error
*/
func (sr SchemaRequest) Validate() error {
	if err := sr.Headers.Validate(); err != nil {
		return err
	}

	// Request key validation
	if sr.Key == nil {
		return errors.New("no request key supplied")
	}
	if err := validateSchemaRequestKey(sr.Key); err != nil {
		return err
	}

	if sr.DatabaseCredentials == nil {
		return errors.New("no database credentials supplied")
	}

	if err := sr.DatabaseCredentials.Validate(); err != nil {
		return err
	}

	return nil
}

/*
Validates the schema request key
	key: *string, the key to be validated
	Return: error, returns a potential error
*/
func validateSchemaRequestKey(key *string) error {
	switch *key {
	case "arango-schema":
		return nil
	case "neo4j-schema":
		return nil
	case "neptune-schema":
		return nil
	default:
		return errors.New("invalid schema request key")
	}
}
