/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package web

import (
	"io/ioutil"
	"net/http"
	"request-handler-service/internal/jwt"
	"request-handler-service/internal/messagequeue"
)

/*
Struct used for JSON conversion
*/
type parsedJSON struct {
	Return    returnStruct
	Entities  []entityStruct
	Relations []relationStruct
}

/*
The struct for returns
*/
type returnStruct struct {
	Entities  []int
	Relations []int
}

/*
The struct for entities
*/
type entityStruct struct {
	Type        string
	Constraints []constraintStruct
}

/*
The struct for relations
*/
type relationStruct struct {
	Type        string
	EntityFrom  int
	EntityTo    int
	Depth       searchDepthStruct
	Constraints []constraintStruct
}

/*
The struct for search dept
*/
type searchDepthStruct struct {
	Min int
	Max int
}

/*
The struct for constraints
*/
type constraintStruct struct {
	Attribute string
	Value     string
	DataType  string
	MatchType string
}

/*
This handles the query request
	w: http.ResponseWriter, the response writer for http
	r: *http.Request, the request handler for http
*/
func queryRequestHandler(w http.ResponseWriter, r *http.Request) {

	setupResponse(&w, r)

	if r.Method == http.MethodPost { // Allow only POST requests

		// Unmarshall request body to interface
		body, err := ioutil.ReadAll(r.Body)

		if err != nil {
			http.Error(w, "bad request body json", http.StatusBadRequest)
			return
		}

		// Get session id from cookie
		cookie, err := r.Cookie("session-token")
		if err != nil {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		cookieValue := &cookie.Value

		valid, sessionID, clientID := jwt.ValidateJWT(*cookieValue)
		if !valid {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		messagequeue.Query(body, sessionID, clientID)

		w.WriteHeader(http.StatusOK)
	} else {
		http.Error(w, "Only POST requests allowed", http.StatusBadRequest)
	}
}
