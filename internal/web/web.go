/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package web

import (
	"encoding/json"
	"errors"
	"io"
	"log"
	"net/http"
)

/*
SetupHTTPServer starts the http listener on port 3000
*/
func SetupHTTPServer() {
	setupHandles()
	log.Println("Started listening on port :3004")
	http.ListenAndServe(":3004", nil)
}

/*
Setups the handles
*/
func setupHandles() {
	http.HandleFunc("/schema/", schemaRequestHandler)
	http.HandleFunc("/query/", queryRequestHandler)
	http.HandleFunc("/create/token/", jwtCreationHandler)
	http.HandleFunc("/database/", databaseHandler)
}

/*
unmarshallBody unmarshalls a request body into the provided struct
	body: *io.readcloser, the body with the reader,
	s: interface{}, an interface for decoding,
	Return: error, returns a potential error
*/
func unmarshallBody(body *io.ReadCloser, s interface{}) error {
	err := json.NewDecoder(*body).Decode(s)

	if err != nil {
		return errors.New("bad request body")
	}
	return nil
}
