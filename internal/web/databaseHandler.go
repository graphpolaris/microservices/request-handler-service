/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package web

import (
	"net/http"
)

/*
The struct for database requests
*/
type databaseRequest struct {
	Key  string              `json:"key"`
	Body databaseCredentials `json:"body"`
}

/*
The struct for database credentials
*/
type databaseCredentials struct {
	DatabaseURL      string `json:"url"`
	DatabasePort     int    `json:"port"`
	DatabaseUsername string `json:"username"`
	DatabasePassword string `json:"password"`
}

/*
This handles the database
	w: http.ResponseWriter, the response writer for http,
	r: *http.Request, the request handler for http
*/
func databaseHandler(w http.ResponseWriter, r *http.Request) {

	setupResponse(&w, r)

	if r.Method == http.MethodPost { // Allow only POST requests

		// Unmarshall request body to QueryRequest struct
		databaseRequest := databaseRequest{}

		if err := unmarshallBody(&r.Body, &databaseRequest); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

	} else {
		http.Error(w, "Only POST requests allowed", http.StatusBadRequest)
	}
}
