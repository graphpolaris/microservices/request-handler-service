/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package web

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"request-handler-service/internal/jwt"
	"request-handler-service/internal/messagequeue"
)

/*
This handles the schema request
	w: http.ResponseWriter, the response writer for http
	r: *http.Request, the request handler for http
*/
func schemaRequestHandler(w http.ResponseWriter, r *http.Request) {

	setupResponse(&w, r)

	if r.Method == http.MethodPost { // Allow only POST requests

		// Unmarshall request body to interface
		body, err := ioutil.ReadAll(r.Body)

		if err != nil {
			http.Error(w, "bad request body json", http.StatusBadRequest)
			return
		}

		// Get session id from cookie
		cookie, err := r.Cookie("session-token")
		if err != nil {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		cookieValue := &cookie.Value

		valid, sessionID, clientID := jwt.ValidateJWT(*cookieValue)
		if !valid {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		var temp map[string]interface{}
		json.Unmarshal(body, &temp)
		databaseName, ok := temp["databaseName"].(string)
		log.Println(databaseName)
		if !ok {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		messagequeue.Schema(body, sessionID, clientID, &databaseName)

		w.WriteHeader(http.StatusOK)
	} else {
		http.Error(w, "Only POST requests allowed", http.StatusBadRequest)
	}
}
