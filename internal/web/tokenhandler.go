/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package web

import (
	"net/http"
	"request-handler-service/internal/jwt"
	"request-handler-service/internal/validate"
)

/*
This handles the jwt creation
	w: http.ResponseWriter, the response writer for http
	r: *http.Request, the request handler for http
*/
func jwtCreationHandler(w http.ResponseWriter, r *http.Request) {
	// Client wants to create a JWT token to encapsulate their clientID, routing key and database credentials
	// They will need to make a post request to this handler with these items included

	setupResponse(&w, r)

	if r.Method == http.MethodPost { // Allow only POST requests

		// Unmarshall request body into token request
		tokenRequest := validate.TokenRequest{}

		if err := unmarshallBody(&r.Body, &tokenRequest); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		if err := tokenRequest.Validate(); err == nil { // Validate this request
			// Create a JWT
			token, err := jwt.Create(&tokenRequest)

			if err != nil {
				http.Error(w, "failed to create JWT", http.StatusBadRequest)
			} else {
				// Send the token
				w.Write([]byte(token))
			}
		} else {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
	} else {
		http.Error(w, "Only POST requests allowed", http.StatusBadRequest)
	}
}
