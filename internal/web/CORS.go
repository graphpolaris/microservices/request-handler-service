/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package web

import "net/http"

/*
SetupResponse sets up response headers for CORS
	w: *http.ResponeWriter, the response writer for hhtp
	r: *http.Request, the request for http
*/
func setupResponse(w *http.ResponseWriter, r *http.Request) {
	// (*w).Header().Set("Access-Control-Allow-Origin", "http://127.0.0.1:3000") // TODO: should be handled by ingress
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	(*w).Header().Set("Access-Control-Allow-Credentials", "true")
}
