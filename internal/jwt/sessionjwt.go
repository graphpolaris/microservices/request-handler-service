/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package jwt

import "github.com/gbrlsnchs/jwt/v3"

/*
SessionIDJWTPayload models a sessionID JWT payload
*/
type SessionIDJWTPayload struct {
	jwt.Payload
	ClientID  string `json:"clientID"`
	SessionID string `json:"sessionID"`
}

/*
ValidateJWT checks if a JWT is valid
	token: string, the token in string form,
	Return: (bool, *string, *string), returns if it has been validated and the session and client ID
*/
func ValidateJWT(token string) (bool, *string, *string) {
	var payload SessionIDJWTPayload
	_, err := jwt.Verify([]byte(token), hsSigning, &payload)

	if err != nil {
		return false, nil, nil
	}

	return true, &payload.SessionID, &payload.ClientID
}
