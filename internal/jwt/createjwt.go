/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package jwt

import (
	"os"
	"request-handler-service/internal/validate"
	"time"

	"github.com/gbrlsnchs/jwt/v3"
)

/*
A type for the jwt payload
*/
type clientJWTPayload struct {
	jwt.Payload
	ClientID string `json:"clientID"`
}

var hsSigning = jwt.NewHS256([]byte(os.Getenv("JWT_SECRET")))

/*
Create creates a signed JWT containing the clientID
	tokenRequest: *validate.TokenRequest, the token request,
	Return: (string, error), returns the token as a string and a potential error
*/
func Create(tokenRequest *validate.TokenRequest) (string, error) {
	jwtPayload := clientJWTPayload{
		Payload: jwt.Payload{
			Issuer:         "GraphPolaris",
			Subject:        "Client token",
			ExpirationTime: jwt.NumericDate(time.Now().Add(24 * 30 * 13 * time.Hour)),
			IssuedAt:       jwt.NumericDate(time.Now()),
		},
		ClientID: *tokenRequest.ClientID,
	}

	token, err := jwt.Sign(jwtPayload, hsSigning) // JWT_SECRET should be Kubernetes shared secret

	if err != nil {
		return "", nil
	}

	return string(token), nil
}

/*
Validate validates a JWT
	token: string, the token in string form
	Return: (string, error), returns the payload client ID and a potential error
*/
func Validate(token string) (string, error) {
	var payload clientJWTPayload
	_, err := jwt.Verify([]byte(token), hsSigning, &payload)

	if err != nil {
		return "", err
	}

	return payload.ClientID, nil
}
