.PHONY: all dep build test lint

lint: dep ## Lint the files
	@golint -set_exit_status ./...

test: dep ## Run unittests
	@go test -cover -coverprofile=coverage.txt -covermode count ./...

race: dep ## Run data race detector
	@go test -race -short ./...

dep: login ## Get the dependencies
	@go get -a -v ./...
	@go get google.golang.org/grpc/internal/transport@v1.37.0
	@go get google.golang.org/grpc@v1.37.0
	@go get -u golang.org/x/lint/golint
	@go get -u golang.org/x/lint/golint
	@go get -u github.com/boumenot/gocover-cobertura

coverage: dep
	@go test -v -coverpkg=./... -coverprofile=cover.out ./...
	@go tool cover -func cover.out | grep total
	@go tool cover -html=cover.out -o cover.html

windows:
	$(eval export GOOS := windows)
	@go build -o builds/main ./cmd/request-handler-service/

macos:
	$(eval export GOOS := darwin)
	@go build -o builds/main ./cmd/request-handler-service/

linux: # Build for linux
	$(eval export GOOS := linux)
	CGO_ENABLED=0 go build -o builds/main ./cmd/request-handler-service/

run:
	./builds/main

develop:
	# RabbitMQ env variables
	$(eval export RABBIT_USER := guest)
	$(eval export RABBIT_PASSWORD := guest)
	$(eval export RABBIT_HOST := localhost)
	$(eval export RABBIT_PORT := 5672)

	# Whether to log
	$(eval export LOG_MESSAGES := true)

	# Redis env variables
	$(eval export REDIS_ADDRESS := localhost:6379)
	$(eval export REDIS_PASSWORD := DevOnlyPass)

	@go run cmd/request-handler-service/main.go

docker:
	make linux
	@docker build -t graphpolaris/request-handler-service:latest .
	@docker push graphpolaris/request-handler-service\:latest

rollout: docker
	kubectl rollout restart deployment request-handler

login:
	echo -e "machine git.science.uu.nl\nlogin gitlab-ci-token\npassword ${CI_JOB_TOKEN}" > ~/.netrc