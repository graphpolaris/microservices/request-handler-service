/*
This program has been developed by students from the bachelor Computer Science at Utrecht University within the Software Project course.
© Copyright Utrecht University (Department of Information and Computing Sciences)
*/

package main

import (
	"request-handler-service/internal/messagequeue"
	"request-handler-service/internal/web"
)

/*
This is the main method, it executes the code for this service
*/
func main() {
	// Setup the web server taking requests
	go web.SetupHTTPServer()

	// Start up the producers
	go messagequeue.Start()

	select {}
}
